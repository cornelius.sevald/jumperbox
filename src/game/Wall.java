package game;

import processing.core.PConstants;

/** Wall / platform class. */
public class Wall extends GameObject {

	static JumperBox jb = JumperBox.jb;

	protected BoxCollider collider;

	// RGB values.
	private int r, g, b = 0;

	/** Construct wall with name, position, scale and RGB values. */
	public Wall(String name, Vector2 position, Vector2 scale, int r, int g, int b) {
		super(name, position, scale);
		collider = new BoxCollider(this);
		setColor(r, g, b);
	}

	/** Construct wall with name, position, scale and color value. */
	public Wall(String name, Vector2 position, Vector2 scale, int value) {
		this(name, position, scale, value, value, value);
	}

	/** Construct wall with position, scale and RGB values. */
	public Wall(Vector2 position, Vector2 scale, int r, int g, int b) {
		this("wall", position, scale, r, g, b);
	}

	/** Construct wall with position, scale and color value. */
	public Wall(Vector2 position, Vector2 scale, int value) {
		this(position, scale, value, value, value);
	}

	/** Construct wall with name and RGB values. */
	public Wall(String name, int r, int g, int b) {
		this(name, Vector2.zero(), Vector2.one(), r, g, b);
	}

	/** Construct wall with name and color value. */
	public Wall(String name, int value) {
		this(name, value, value, value);
	}

	/** Construct wall with RGB values. */
	public Wall(int r, int g, int b) {
		this(Vector2.zero(), Vector2.one(), r, g, b);
	}

	/** Construct wall with color value. */
	public Wall(int value) {
		this(value, value, value);
	}

	/** Get the RGB colors as an array. */
	public int[] getColor() {
		int[] rgb = { r, g, b };
		return rgb;
	}

	/**
	 * Set the RGB color of the object.
	 * 
	 * 'r', 'g' and 'b' must be in the range [0 : 256[.
	 */
	public void setColor(int r, int g, int b) {
		if (r < 0 || r >= 256 || g < 0 || g >= 256 || b < 0 || b >= 256) {
			throw new java.lang.IllegalArgumentException("'r', 'g' and " + "'b' must be in the range [0 : 256[");
		}
		this.r = r;
		this.g = g;
		this.b = b;
	}

	/* Set the RGB color of the object all equal to value. */
	public void setColor(int value) {
		if (value < 0 || value >= 256) {
			throw new java.lang.IllegalArgumentException("'value' must " + "be in the range [0 : 256[");
		}
		r = g = b = value;
	}
	
	@Override
	public void delete() {
		collider.delete();
		super.delete();
	}

	/** Update the wall. */
	@Override
	public void update() {
		show();
	}

	/** Show the wall. */
	public void show() {
		Vector2 screenPosition = transform.toScreenPoint();
		Vector2 screenScale = transform.toScreenScale();
		
		jb.stroke(0);
		jb.strokeWeight(1);
		jb.fill(r, g, b);
		jb.rectMode(PConstants.CENTER);
		jb.rect(screenPosition.x, screenPosition.y, screenScale.x, screenScale.y);
	}
}
