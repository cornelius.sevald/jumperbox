package game;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PConstants;

public class Sign extends Text {

	static String imagePath = "data/sign.png";
	
	BoxCollider collider;
	PImage signImage;
	Vector2 shift;
	
	
	/** Create a sign instance with a name, position, scale, text shift and info and a message to be displayed. */
	public Sign(String name, Vector2 position, Vector2 scale, Vector2 shift, String fontName, float fontSize, String message) {
		super(name, position, scale, PConstants.CENTER, PConstants.CENTER, fontName, fontSize, message);
		collider = new BoxCollider(this, true);
		this.shift = shift;

		signImage = jb.loadImage(imagePath);
		signImage.resize((int) Transform.toScreenScale(scale).x, -(int) Transform.toScreenScale(scale).y);
	}
	
	public Sign(Vector2 position, Vector2 scale, Vector2 shift, String fontName, float fontSize, String message) {
		this("sign", position, scale, shift, fontName, fontSize, message);
	}
	
	public Sign(String name, Vector2 position, Vector2 scale, Vector2 shift, float fontSize, String message) {
		this(name, position, scale, shift, "Verdana", fontSize, message);
	}
	
	public Sign(Vector2 position, Vector2 scale, Vector2 shift, float fontSize, String message) {
		this("sign", position, scale, shift, fontSize, message);
	}
	
	public Sign(String name, Vector2 position, Vector2 scale, String fontName, float fontSize, String message) {
		this(name, position, scale, Vector2.zero(), fontName, fontSize, message);
	}
	
	public Sign(Vector2 position, Vector2 scale, String fontName, float fontSize, String message) {
		this("name", position, scale, Vector2.zero(), fontName, fontSize, message);
	}
	
	public Sign(String name, Vector2 position, Vector2 scale, float fontSize, String message) {
		this(name, position, scale, Vector2.zero(), fontSize, message);
	}
	
	public Sign(Vector2 position, Vector2 scale, float fontSize, String message) {
		this("sign", position, scale, fontSize, message);
	}

	@Override
	public void update() {
		show();
		if (Bounds.overlaps(collider.bounds(), Level.loadedLevel.player.getCollider().bounds())) {
			showText();
		}
	}
	
	@Override
	public void delete() {
		super.delete();
		collider.delete();
	}

	@Override
	void show() {
		jb.imageMode(PApplet.CENTER);
		jb.image(signImage, transform.toScreenPoint().x,
				 transform.toScreenPoint().y);
	}
	void showText() {
		jb.rectMode(PConstants.CENTER);
		jb.fill(210,105,30, 100);
		jb.rect(transform.toScreenPoint().x + Transform.toScreenScale(shift).x, 
				transform.toScreenPoint().y + transform.toScreenScale().y * 0.95f + 
				Transform.toScreenScale(shift).y, 
				message.length() * 0.6f * font.getSize(), font.getSize() * 1.2f);
		jb.fill(0);
		jb.textFont(font,fontSize);
		jb.textAlign(PConstants.CENTER, PConstants.CENTER);
		jb.text(message, transform.toScreenPoint().x + Transform.toScreenScale(shift).x, 
				transform.toScreenPoint().y + transform.toScreenScale().y + Transform.toScreenScale(shift).y);
	}
}
