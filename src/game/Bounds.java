package game;

import java.util.ArrayList;

import processing.core.PApplet;

/** Axis-aligned bounding box (AABB) class. */
public class Bounds {
	
	private Vector2 centre, size;
	
	/** Return true if the two boxes overlap. */
	public static boolean overlaps(Bounds b1, Bounds b2) {
		if (b1.getMax().x < b2.getMin().x) {return false;} // a is left of b
	    if (b1.getMin().x > b2.getMax().x) {return false;} // a is right of b
	    if (b1.getMax().y < b2.getMin().y) {return false;} // a is above b
	    if (b1.getMin().y > b2.getMax().y) {return false;} // a is below b
	    return true; // boxes overlap
	}
	
	/** Construct new Bounds instance. */
	public Bounds(Vector2 centre, Vector2 size) {
		setCentre(centre);
		setSize(size);
	}
	
	/** Get the centre of the box. */
	public Vector2 getCentre() {
		return centre;
	}
	
	/** Set the centre of the box. */
	public void setCentre(Vector2 centre) {
		this.centre = centre;
	}
	
	/** Get the size of the box. Equal to twice the extents. */
	public Vector2 getSize() {
		return size;
	}
	
	/** Set the size of the box. */
	public void setSize(Vector2 size) {
		if (size.x < 0 || size.y < 0) {
			throw new java.lang.IllegalArgumentException("Negative coordinates of size is not allowed.");
		}
		
		this.size = size;
	}
	
	/** Get the extents of the box. Equal to half the size. */
	public Vector2 getExtents() {
		return size.div(2);
	}
	
	public void setExtents(Vector2 extents) {
		setSize(extents.mul(2));
	}
	
	/** Get the maximal point of the box. Equal to centre+extents. */
	public Vector2 getMax() {
		return centre.add(getExtents());
	}
	
	/** Get the minimal point of the box. Equal to centre-extents. */
	public Vector2 getMin() {
		return centre.sub(getExtents());
	}
	
	/** Set the minimal and maximal point of the bounds. */
	public void setMinMax(Vector2 min, Vector2 max) {
		if (min.x > max.x || min.y > max.y) {
			throw new java.lang.IllegalArgumentException("Both coordinates of the maximal point"
					+ " must be grater than those of the minimal.");
		}
		
		float centreX = PApplet.lerp(min.x, max.x, 0.5f);
		float centreY = PApplet.lerp(min.y, max.y, 0.5f);
		float sizeX = max.x - min.x;
		float sizeY = max.y - min.y;
		
		centre = new Vector2(centreX, centreY);
		size = new Vector2(sizeX, sizeY);
	}
	
	/** Grow the size of bounds by amount along each axis. */
	public void expand(Vector2 amount) {
		Vector2 newSize = new Vector2(size.x, size.y);
		newSize.x += amount.x;
		newSize.y += amount.y;
		
		setSize(newSize);
	}
	
	/** Grow the size of bounds by amount along each axis. */
	public void expand(float amount) {
		expand(Vector2.one().mul(amount));
	}
	
	/** Is the point inside the box? Does not count points on the edge. */
	public boolean contains(Vector2 point) {
		boolean xInside = getMin().x < point.x &&
				point.x < getMax().x;
		boolean yInside = getMin().y < point.y &&
				point.y < getMax().y;
		
		return xInside && yInside;
	}
	
	/** Is the point on the edge of the box? */
	public boolean isPointOnEdge(Vector2 point) {
		boolean xOnEdge = getMin().x == point.x ||
				point.x == getMax().x;
		boolean yOnEdge = getMin().y == point.y ||
				point.y == getMax().y;
		
		return xOnEdge || yOnEdge;
	}
	
	/** Return the closest point on the box. */
	public Vector2 closestPoint(Vector2 point) {
		if (contains(point) || isPointOnEdge(point)) {
			return point;
		}
		
		float dx, dy;
		// Find dx.
		if (point.x - getMax().x < 0) {
			dx = PApplet.max(getMin().x - point.x, 0f);
		}
		else {
			dx = getMax().x - point.x;
		}
		// Find dy.
		if (point.y - getMax().y < 0) {
			dy = PApplet.max(getMin().y - point.y, 0f);
		}
		else {
			dy = getMax().y - point.y;
		}
		Vector2 closestPoint = new Vector2(point.x + dx, point.y + dy);
		
		return closestPoint;
	}
	
	/** Return an array with all the intersecton 
	 * points between the ray and this bounding box.
	 * The points are sorted from closest to the ray origin to the farthest.
	 * */
	public Vector2[] rayIntersection(Ray ray) {
		// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
		
		Vector2 B0 = getMin();
		Vector2 B1 = getMax();
		Vector2 O = ray.getOrigin();
		Vector2 D = ray.getDirection();
		ArrayList<Vector2> intersections = new ArrayList<Vector2>();
		
		// Compute the point along the ray where the ray intersects
		// the lines defined by the boxes segments.
		float t0x = (B0.x - O.x) / D.x;
		float t1x = (B1.x - O.x) / D.x;
		float t0y = (B0.y - O.y) / D.y;
		float t1y = (B1.y - O.y) / D.y;
		
		// Convert them into actual points.
		Vector2 T0x = ray.getPoint(t0x);
		Vector2 T1x = ray.getPoint(t1x);
		Vector2 T0y = ray.getPoint(t0y);
		Vector2 T1y = ray.getPoint(t1y);
		
		// Check of the points lie between the corners.
		// If they do, they intersect the line segment.
		// Check for T0x.
		for (int i = 0; i < 4; i++) {
			Vector2 T;
			float t;
			float Tc;
			float B0c;
			float B1c;
			if (i == 0) {
				T = T0x;
				t = t0x;
				Tc = T.y;
				B0c = B0.y;
				B1c = B1.y;
			}
			else if (i == 1) {
				T = T1x;
				t = t1x;
				Tc = T.y;
				B0c = B0.y;
				B1c = B1.y;
			}
			else if (i == 2) {
				T = T0y;
				t = t0y;
				Tc = T.x;
				B0c = B0.x;
				B1c = B1.x;
			}
			else {
				T = T1y;
				t = t1y;
				Tc = T.x;
				B0c = B0.x;
				B1c = B1.x;
			}
			
			float alpha = (Tc-B0c)/(B1c-B0c);
			if (0 <= alpha && alpha <= 1 && 0 <= t) {
				// Subtract ray origin for sorting.
				intersections.add(T.sub(ray.getOrigin()));
			}
		}
		// Sort the vectors.
		intersections.sort(new MagnitudeComparator());
		// Add the ray origin.
		for (int i = 0; i < intersections.size(); i++) {
			intersections.set(i, intersections.get(i).add(ray.getOrigin()));
		}
		
		Vector2[] intersectionsArray = new Vector2[intersections.size()];
		return intersections.toArray(intersectionsArray);
	}
}
