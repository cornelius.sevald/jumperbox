package game;

import java.util.Comparator;

// Info about a raycast hit.
public class HitInfo {
	
	// The collider that was hit.
	Collider collider;
	// The distance to the hit point from the ray.
	float distance;
	// The point that was hit.
	Vector2 point;
	
	public HitInfo(Collider collider, float distance, Vector2 point) {
		this.collider = collider;
		this.distance = distance;
		this.point = point;
	}
	
	public Collider getCollider() {
		return collider;
	}
	public float getDistance() {
		return distance;
	}
	public Vector2 getPoint() {
		return point;
	}
}

/** Compare vectors based off of magnitude. */
class HitDistanceComparator implements Comparator<HitInfo> {
	
	@Override
	public int compare(HitInfo o1, HitInfo o2) {
		float mag1 = o1.getDistance();
		float mag2 = o2.getDistance();

		int result = mag1 <  mag2 ? -1 : mag1 == mag2 ? 0 : 1;
		return result;
	}
}
