package game;

/** Position and scale of an object.
 * 
 * Rotation is not implemented unless it is.
 *  */
public class Transform {
	
	public static JumperBox jb = JumperBox.jb;
	
	// Units per width.
	static int UPW = 16;
	// Units per height.
	static int UPH = 9;
	
	private GameObject gameObject;
	public Vector2 position, scale;
	
	/** Construct a transform attached to a gameObject with a position and scale. */
	public Transform(GameObject gameObject, Vector2 position, Vector2 scale) {
		this.gameObject = gameObject;
		this.position = position;
		this.scale = scale;
	}
	
	/** Construct a transform attached to a gameObject with default position and scale. */
	public Transform(GameObject gameObject) {
		this(gameObject, Vector2.zero(), Vector2.one());
	}
	
	/** Get the GameObject this transform is attached to. */
	public GameObject getGameObject() {
		return gameObject;
	}
	
	/** [OBSOLETE: use public field instead]
	 * Get the position of the transform. */
	public Vector2 getPosition() {
		return position;
	}
	
	/** [OBSOLETE: use public field instead]
	 * Set the position of the transform. */
	public void setPosition(Vector2 position) {
		this.position = position;
	}
	
	/** [OBSOLETE: use public field instead]
	 * Get the scale of the transform. */
	public Vector2 getScale() {
		return scale;
	}
	
	/** [OBSOLETE: use public field instead]
	 * Set the scale of the transform.
	 * */
	public void setScale(Vector2 scale) {
		this.scale = scale;
	}
	
	// Return the position of the transform converted to screen points.
	public Vector2 toScreenPoint() {
		return toScreenPoint(this.position);
	}
	
	// Return the scale of the transform converted to screen scale.
	public Vector2 toScreenScale() {
		return toScreenScale(this.scale);
	}
	
	/** Move position in the direction and length of 'translation'. */
	public void translate(Vector2 translation) {
		position = position.add(translation);
	}
	
	public static Vector2 toScreenPoint(Vector2 worldPoint) {
		Vector2 screenPoint = new Vector2();
		screenPoint.x = worldPoint.x * jb.width / UPW;
		screenPoint.y = jb.height - worldPoint.y * jb.height / UPH;
		
		return screenPoint;
	}
	
	public static Vector2 toWorldPoint(Vector2 screenPoint) {
		Vector2 worldPoint = new Vector2();
		worldPoint.x = screenPoint.x * UPW / jb.width;
		worldPoint.y = (jb.height - screenPoint.y) * UPH / jb.height;
		
		return screenPoint;
	}
	
	public static Vector2 toScreenScale(Vector2 worldScale) {
		Vector2 screenScale = new Vector2();
		screenScale.x = worldScale.x * jb.width / UPW;
		screenScale.y = -worldScale.y * jb.height / UPH;
		
		return screenScale;
	}
	
	public static Vector2 toWorldScale(Vector2 screenScale) {
		Vector2 worldScale = new Vector2();
		worldScale.x = screenScale.x * UPW / jb.width;
		worldScale.y = -screenScale.y * UPH / jb.height;
		
		return worldScale;
	}
}
