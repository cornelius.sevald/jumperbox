package game;

import java.time.LocalTime;
import java.io.File;
import java.util.*;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import processing.core.PConstants;

/**
 * A game level consisting of an amount of game objects, a spawn point and a
 * player.
 */
public class Level {

	static JumperBox jb = JumperBox.jb;

	static final int LEVELCOUNT = 5;
	// Standard jump height.
	static final float STDJUMP = 1.9f;

	// The level that is currently loaded.
	static Level loadedLevel;
	// The index of the loaded level.
	static int loadedIndex;
	// The amount of attempts at the currently loaded level. Starts at 0.
	static int attempts;
	// The song currently playing.
	static Clip bgMusic;
	// The path to that song
	static String bgMusicPath;

	public ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
	// Set of objects to delete.
	public Set<GameObject> toDelete = new HashSet<GameObject>();

	// Has the level been won?
	public boolean won = false;

	Player player;

	/** Unload the current level, and load the one with this index instead. */
	public static void loadLevel(int index) {

		JumperBox.playing = false;
		double startLoadTime = System.currentTimeMillis();

		if (loadedLevel != null) {
			loadedLevel.unload();
		}
		if (loadedIndex != index) {
			attempts = 0;
		}
		loadedLevel = generateLevel(index);
		loadedIndex = index;

		generateOuterWalls(true, false, true, true);
		loadedLevel.getPlayer().setup();

		if (0 <= index && index < LEVELCOUNT) {
			if (index == 4) {
				jb.updateDiscordPresence("Har le dårlig tid \ud83d\ude33", String.format("På level %d", index + 1),
						"flushed-sphere", "jumpman-aware");
			} else {
				jb.updateDiscordPresence("Gamer epic style \ud83d\ude0e", String.format("På level %d", index + 1),
						"sunglasses-sphere", "jumpman");
				jb.discordLib.Discord_UpdatePresence(jb.presence);
			}
		} else {
			jb.updateDiscordPresence("Vandt computerspillet",
					String.format("\u2665 %d\n\u231b %s", jb.attempts,
							LocalTime.MIN.plusSeconds((int) (jb.getMilliTimePlayed() / 1000)).toString()),
					"triumph-sphere", "jumpman");
		}

		double endLoadTime = System.currentTimeMillis();
		jb.pauseTime += endLoadTime - startLoadTime;
		JumperBox.playing = true;
	}

	public static void loadNext() {
		loadLevel(loadedIndex + 1);
	}

	public static void reload() {
		attempts++;
		jb.attempts++;
		loadLevel(loadedIndex);
	}

	// Surround the screen with four walls.
	static void generateOuterWalls(boolean down, boolean up, boolean left, boolean right) {
		float wallThickness = 1f;

		// Calculate sizes of walls.
		Vector2 horizontalSize = new Vector2(Transform.UPW, wallThickness);
		Vector2 verticalSize = new Vector2(wallThickness, Transform.UPH * 2);

		// Calculate centers of walls.
		Vector2 downCentre = Vector2.down().mul(wallThickness * .5f).add(Vector2.right().mul(Transform.UPW * .5f));
		Vector2 upCentre = downCentre.add(Vector2.up().mul(wallThickness + Transform.UPH));
		Vector2 leftCentre = Vector2.left().mul(wallThickness * .5f).add(Vector2.up().mul(Transform.UPH * .5f));
		Vector2 rightCentre = leftCentre.add(Vector2.right().mul(wallThickness + Transform.UPW));

		if (down) {
			loadedLevel.gameObjects.add(new Wall(downCentre, horizontalSize, 200, 100, 100));
		}
		if (up) {
			loadedLevel.gameObjects.add(new Wall(upCentre, horizontalSize, 100, 200, 100));
		}
		if (left) {
			loadedLevel.gameObjects.add(new Wall(leftCentre, verticalSize, 100, 100, 200));
		}
		if (right) {
			loadedLevel.gameObjects.add(new Wall(rightCentre, verticalSize, 200, 100, 200));
		}
	}

	// ** Construct new level instance. */
	public Level(GameObject[] gameObjects, Player player, String soundPath, int soundLoops) {
		this.gameObjects = new ArrayList<GameObject>(Arrays.asList(gameObjects));
		this.player = player;
		
		if (jb.gOptions.music && bgMusicPath != soundPath) {
			if (bgMusic != null) {
				bgMusic.stop();
				bgMusic.close();
			}
			try {
				File adioFile = new File(soundPath);
				AudioInputStream inputStream = AudioSystem.getAudioInputStream(adioFile);
				bgMusic = AudioSystem.getClip();
				bgMusic.open(inputStream);

				bgMusic.loop(soundLoops);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bgMusicPath = soundPath;
		}
	}

	// ** Construct new level instance. */
	public Level(GameObject[] gameObjects, Player player) {
		this(gameObjects, player, "", 0);
	}

	public Player getPlayer() {
		return player;
	}

	public void updateObjects() {
		for (GameObject gm : gameObjects) {
			gm.update();
		}
		player.update();
		gameObjects.removeAll(toDelete);

		if (loadedLevel.won) {
			loadNext();
		} else if (!player.alive) {
			reload();
		}
	}

	public void unload() {
		for (GameObject gm : gameObjects) {
			gm.delete();
		}
		player.delete();
	}

	// Put all level-data here.
	public static Level generateLevel(int index) {
		if (index == 0) {
			// Load first level.
			return new Level(
					new GameObject[] { new Wall(new Vector2(8, 0.5f), new Vector2(16, 1), 0),
							new Wall(new Vector2(15.75f, 3f), new Vector2(0.75f, 7f), 0),
							new Wall(new Vector2(11, 2.5f), new Vector2(3.25f, 0.5f), 0),
							new Wall(new Vector2(3.5f, 4f), new Vector2(4f, 0.5f), 0),
							new Wall(new Vector2(2f, 5.75f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(8, 6.5f), new Vector2(3.25f, 0.5f), 0),
							new Wall(new Vector2(14f, 6.125f), new Vector2(3f, 0.75f), 0),
							new Exit(new Vector2(15.5f, 7.75f), new Vector2(1, 2.5f)),
							new Sign(new Vector2(13.5f, 7.2f), new Vector2(1.25f, 1.4f), new Vector2(-1, 0), 48,
									"Brug tasterne til at bev\u00E6ge dig") },
					new Player(5, 20, STDJUMP, new Vector2(4, 2f), Vector2.one(), 210, 105, 30), "data/Ocean.wav",
					Clip.LOOP_CONTINUOUSLY);
		} else if (index == 1) {
			// load second level.
			Wall disposableWall1 = new Wall(new Vector2(7.5f, 1.625f), new Vector2(1f, 1.25f), 50, 50, 150);
			return new Level(
					new GameObject[] { new Wall(new Vector2(8, 0.5f), new Vector2(16, 1), 0),
							new Wall(new Vector2(15.75f, 3f), new Vector2(0.75f, 7f), 0),
							new Wall(new Vector2(7.5f, 5.75f), new Vector2(1f, 7f), 0),
							new Wall(new Vector2(6f, 2.5f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(1f, 4f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(6f, 5.5f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(1f, 7f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(14.9f, 1.5f), new Vector2(1.05f, 1f), 0),
							new Wall(new Vector2(12f, 3.5f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(9f, 5f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(14f, 6.125f), new Vector2(3f, 0.75f), 0), disposableWall1,
							new Lever(new Vector2(0.75f, 7.65f), new Vector2(1, 1),
									new GameObject[] { disposableWall1 }),
							new Exit(new Vector2(15.5f, 7.75f), new Vector2(1, 2.5f)),
							new Sign(new Vector2(13.5f, 7.2f), new Vector2(1.25f, 1.4f), new Vector2(-1, 0), 48,
									"Tr\u00E6k i h\u00E5ndtaget Kronk") },
					new Player(5, 20, STDJUMP, new Vector2(2, 2f), Vector2.one(), 210, 105, 30), "data/Ocean.wav",
					Clip.LOOP_CONTINUOUSLY);
		} else if (index == 2) {
			// Load third level
			return new Level(
					new GameObject[] { new Wall(new Vector2(3f, 6f), new Vector2(6f, 0.5f), 0),
							new Wall(new Vector2(5.75f, 5f), new Vector2(0.5f, 2f), 0),
							new Wall(new Vector2(8f, 4.25f), new Vector2(4f, 0.5f), 0),
							new Wall(new Vector2(9.75f, 5f), new Vector2(0.5f, 2f), 0),
							new Wall(new Vector2(11f, 6f), new Vector2(3f, 0.5f), 0),
							new Wall(new Vector2(12.25f, 3f), new Vector2(0.5f, 6f), 0),
							new Wall(new Vector2(14f, 0.5f), new Vector2(4f, 1f), 0),
							new Wall(new Vector2(14f, 6.5f), new Vector2(0.5f, 6f), 0),
							new Wall(new Vector2(15f, 3.75f), new Vector2(2f, 0.5f), 0),
							new Wall(new Vector2(7.75f, 7.7f), new Vector2(1f, 3f), 0),
							new Sans(new Vector2(6.75f, 5f), new Vector2(1, 1),
									new Vector2[] { new Vector2(6.75f, 5f), new Vector2(8.75f, 5f),
											new Vector2(8.75f, 8f), new Vector2(6.75f, 8f) },
									Sans.MoveType.LOOP, 4f),

							new Sans(new Vector2(2.75f, 3.75f), new Vector2(1, 1),
									new Vector2[] { new Vector2(2.75f, 2.75f), new Vector2(2.75f, 4.75f),
											new Vector2(4.5f, 4.75f) },
									Sans.MoveType.BOUNCE, 2f),
							new Sans(new Vector2(2.75f, 3.75f), new Vector2(1, 1),
									new Vector2[] { new Vector2(2.75f, 2.75f), new Vector2(4.75f, 2.75f),
											new Vector2(4.75f, 0.75f) },
									Sans.MoveType.BOUNCE, 2f),
							new Sans(new Vector2(2.75f, 3.75f), new Vector2(1, 1),
									new Vector2[] { new Vector2(2.75f, 2.75f), new Vector2(2.75f, 0.75f),
											new Vector2(0.75f, 0.75f) },
									Sans.MoveType.BOUNCE, 2f),
							new Sans(new Vector2(2.75f, 3.75f), new Vector2(1, 1),
									new Vector2[] { new Vector2(2.75f, 2.75f), new Vector2(0.75f, 2.75f),
											new Vector2(0.75f, 4.75f) },
									Sans.MoveType.BOUNCE, 2f),
							new Sans(new Vector2(11f, 1f), new Vector2(1, 1),
									new Vector2[] { new Vector2(11f, 1f), new Vector2(11f, 5f) }, Sans.MoveType.BOUNCE,
									3f),
							new Sans(new Vector2(9f, 3f), new Vector2(1, 1),
									new Vector2[] { new Vector2(9f, 3f), new Vector2(6f, 3f) }, Sans.MoveType.BOUNCE,
									3f),

							new Exit(new Vector2(15.5f, 2.25f), new Vector2(1, 2.5f)),
							new Sign(new Vector2(13.5f, 1.7f), new Vector2(1.25f, 1.4f), new Vector2(-1, 0), 48,
									"Du stjal deres ketchup >:("),
							new Ketchup(new Vector2(11f, 6.95f), new Vector2(1f, 1.4f)),
							new Text(new Vector2(0.25f, 7.75f), Vector2.one(), PConstants.LEFT, PConstants.CENTER,
									"Consolas", 30, String.format("\u2665 %d", attempts)) },
					new Player(5, 20, STDJUMP, new Vector2(2, 7f), Vector2.one(), 210, 105, 30), "data/Ocean.wav",
					Clip.LOOP_CONTINUOUSLY);
		} else if (index == 3) {
			// load fourth level
			Wall disposableWall2 = new Wall(new Vector2(14.5f, 1.375f), new Vector2(1f, 0.25f), 50, 50, 150);
			Wall disposableWall3 = new Wall(new Vector2(6.75f, 7.375f), new Vector2(1f, 0.25f), 50, 50, 150);
			return new Level(
					new GameObject[] { new Wall(new Vector2(8f, 0.25f), new Vector2(16f, 0.5f), 0),
							new Wall(new Vector2(7.875f, 1.375f), new Vector2(12.25f, 0.25f), 0),
							new Wall(new Vector2(15.5f, 1.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(3.25f, 2.375f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(6.25f, 2.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(10.25f, 2.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(12f, 2.125f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(15f, 2.615f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(11f, 2.875f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(13f, 2.875f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(3.875f, 3.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(5f, 3.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(7.5f, 3.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(9f, 3.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(12f, 3.625f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(3.5f, 4.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(6.25f, 4.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(9.75f, 4.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(11f, 4.375f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(13.625f, 4.615f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(12f, 4.875f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(2.375f, 5.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(3.875f, 5.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(5f, 5.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(8.625f, 5.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(11.5f, 5.625f), new Vector2(3.5f, 0.25f), 0),
							new Wall(new Vector2(14.875f, 5.625f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(7.25f, 6.125f), new Vector2(2f, 0.25f), 0),
							new Wall(new Vector2(3.5f, 6.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(7f, 6.375f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(12.75f, 6.625f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(0.5f, 7.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(2.625f, 7.375f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(5.375f, 7.375f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(8.625f, 7.375f), new Vector2(2.75f, 0.25f), 0),
							new Wall(new Vector2(14.f, 7.375f), new Vector2(2f, 0.25f), 0),
							new Wall(new Vector2(11.5f, 8f), new Vector2(1.5f, 1f), 0),
							new Wall(new Vector2(8f, 8.75f), new Vector2(16f, 0.5f), 0),

							new Wall(new Vector2(0.875f, 3.875f), new Vector2(0.25f, 6.75f), 0),
							new Wall(new Vector2(1.875f, 4.375f), new Vector2(0.25f, 5.75f), 0),
							new Wall(new Vector2(2.875f, 3.25f), new Vector2(0.25f, 2f), 0),
							new Wall(new Vector2(4.375f, 4.875f), new Vector2(0.25f, 5.25f), 0),
							new Wall(new Vector2(7.375f, 6.75f), new Vector2(0.25f, 1f), 0),
							new Wall(new Vector2(8.125f, 3.75f), new Vector2(0.25f, 4.5f), 0),
							new Wall(new Vector2(9.875f, 6.5f), new Vector2(0.25f, 1.5f), 0),
							new Wall(new Vector2(10.625f, 3.875f), new Vector2(0.25f, 3.25f), 0),
							new Wall(new Vector2(13.125f, 6.5f), new Vector2(0.25f, 1.5f), 0),
							new Wall(new Vector2(13.375f, 3f), new Vector2(0.25f, 3f), 0),
							new Wall(new Vector2(14.375f, 4f), new Vector2(0.25f, 1f), 0),
							new Wall(new Vector2(15.875f, 5f), new Vector2(0.25f, 7f), 0),

							new Sans(new Vector2(1.375f, 1f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(1.375f, 1f), new Vector2(1.375f, 2f) },
									Sans.MoveType.BOUNCE, 3f),
							new Sans(new Vector2(3.575f, 8f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(3.575f, 8f), new Vector2(7.375f, 8f) },
									Sans.MoveType.BOUNCE, 4f),
							new Sans(new Vector2(3.875f, 5.9f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(3.875f, 5.9f), new Vector2(2.375f, 5.9f) },
									Sans.MoveType.BOUNCE, 4f),
							new Sans(new Vector2(7.375f, 1.9f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(7.375f, 1.9f), new Vector2(2.375f, 1.9f) },
									Sans.MoveType.BOUNCE, 4f),
							new Sans(new Vector2(5f, 4f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(5f, 4f), new Vector2(7.5f, 5.5f) },
									Sans.MoveType.BOUNCE, 4f),
							new Sans(new Vector2(10.5f, 6.1f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(10.5f, 6.1f), new Vector2(12.5f, 6.1f) },
									Sans.MoveType.BOUNCE, 4f),
							new Sans(new Vector2(15.25f, 6.1f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(15.375f, 6.1f), new Vector2(13.625f, 6.1f),
											new Vector2(13.625f, 5.1f), new Vector2(15.375f, 5.1f),
											new Vector2(15.375f, 3.1f) },
									Sans.MoveType.BOUNCE, 6f),
							new Sans(
									new Vector2(11.125f, 4.85f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(11.125f, 4.85f), new Vector2(11.125f, 3.35f) },
									Sans.MoveType.BOUNCE, 4f),

							disposableWall2, disposableWall3,
							new Lever(new Vector2(4f, 3.7f), new Vector2(0.5f, 0.5f),
									new GameObject[] { disposableWall3 }),
							new Lever(new Vector2(7.75f, 6.45f), new Vector2(0.5f, 0.5f),
									new GameObject[] { disposableWall2 }),
							new Sign(new Vector2(14.5f, 0.85f), new Vector2(0.625f, 0.7f), new Vector2(-1, 0), 38,
									"Du kommer til at have \n le d\u00E5rlig tid"),
							new Exit(new Vector2(15.75f, 0.875f), new Vector2(0.5f, 0.75f)),
							new Text(new Vector2(0.25f, 7.75f), Vector2.one(), 255, PConstants.LEFT, PConstants.CENTER,
									"Consolas", 30, String.format("\u2665 %d", attempts)) },
					new Player(3, 20, 1.15f, new Vector2(0.5f, 7.75f), new Vector2(0.5f, 0.5f), 210, 105, 30),
					"data/sans-song.wav", Clip.LOOP_CONTINUOUSLY);
		} else if (index == 4) {
			// load "fifth" level
			Wall disposableWall2 = new Wall(new Vector2(14.5f, 1.375f), new Vector2(1f, 0.25f), 50, 50, 150);
			Wall disposableWall3 = new Wall(new Vector2(6.75f, 7.375f), new Vector2(1f, 0.25f), 50, 50, 150);
			return new Level(
					new GameObject[] { new Wall(new Vector2(8f, 0.25f), new Vector2(16f, 0.5f), 0),
							new Wall(new Vector2(7.875f, 1.375f), new Vector2(12.25f, 0.25f), 0),
							new Wall(new Vector2(15.5f, 1.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(3.25f, 2.375f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(6.25f, 2.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(10.25f, 2.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(12f, 2.125f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(15f, 2.615f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(11f, 2.875f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(13f, 2.875f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(3.875f, 3.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(5f, 3.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(7.5f, 3.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(9f, 3.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(12f, 3.625f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(3.5f, 4.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(6.25f, 4.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(9.75f, 4.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(11f, 4.375f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(13.625f, 4.615f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(12f, 4.875f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(2.375f, 5.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(3.875f, 5.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(5f, 5.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(8.625f, 5.375f), new Vector2(0.75f, 0.25f), 0),
							new Wall(new Vector2(11.5f, 5.625f), new Vector2(3.5f, 0.25f), 0),
							new Wall(new Vector2(14.875f, 5.625f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(7.25f, 6.125f), new Vector2(2f, 0.25f), 0),
							new Wall(new Vector2(3.5f, 6.375f), new Vector2(1.5f, 0.25f), 0),
							new Wall(new Vector2(7f, 6.375f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(12.75f, 6.625f), new Vector2(0.5f, 0.25f), 0),
							new Wall(new Vector2(0.5f, 7.375f), new Vector2(1f, 0.25f), 0),
							new Wall(new Vector2(2.625f, 7.375f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(5.375f, 7.375f), new Vector2(1.75f, 0.25f), 0),
							new Wall(new Vector2(8.625f, 7.375f), new Vector2(2.75f, 0.25f), 0),
							new Wall(new Vector2(14.f, 7.375f), new Vector2(2f, 0.25f), 0),
							new Wall(new Vector2(11.5f, 8f), new Vector2(1.5f, 1f), 0),
							new Wall(new Vector2(8f, 8.75f), new Vector2(16f, 0.5f), 0),

							new Wall(new Vector2(0.875f, 3.875f), new Vector2(0.25f, 6.75f), 0),
							new Wall(new Vector2(1.875f, 4.375f), new Vector2(0.25f, 5.75f), 0),
							new Wall(new Vector2(2.875f, 3.25f), new Vector2(0.25f, 2f), 0),
							new Wall(new Vector2(4.375f, 4.875f), new Vector2(0.25f, 5.25f), 0),
							new Wall(new Vector2(7.375f, 6.75f), new Vector2(0.25f, 1f), 0),
							new Wall(new Vector2(8.125f, 3.75f), new Vector2(0.25f, 4.5f), 0),
							new Wall(new Vector2(9.875f, 6.5f), new Vector2(0.25f, 1.5f), 0),
							new Wall(new Vector2(10.625f, 3.875f), new Vector2(0.25f, 3.25f), 0),
							new Wall(new Vector2(13.125f, 6.5f), new Vector2(0.25f, 1.5f), 0),
							new Wall(new Vector2(13.375f, 3f), new Vector2(0.25f, 3f), 0),
							new Wall(new Vector2(14.375f, 4f), new Vector2(0.25f, 1f), 0),
							new Wall(new Vector2(15.875f, 5f), new Vector2(0.25f, 7f), 0),

							new Sans(new Vector2(1.375f, 1f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(1.375f, 1f), new Vector2(1.375f, 2f) },
									Sans.MoveType.BOUNCE, 3f / 3f),
							new Sans(new Vector2(3.575f, 8f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(3.575f, 8f), new Vector2(7.375f, 8f) },
									Sans.MoveType.BOUNCE, 4f / 3f),
							new Sans(new Vector2(3.875f, 5.9f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(3.875f, 5.9f), new Vector2(2.375f, 5.9f) },
									Sans.MoveType.BOUNCE, 4f / 3f),
							new Sans(new Vector2(7.375f, 1.9f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(7.375f, 1.9f), new Vector2(2.375f, 1.9f) },
									Sans.MoveType.BOUNCE, 4f / 3f),
							new Sans(new Vector2(5f, 4f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(5f, 4f), new Vector2(7.5f, 5.5f) },
									Sans.MoveType.BOUNCE, 4f / 3f),
							new Sans(new Vector2(10.5f, 6.1f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(10.5f, 6.1f), new Vector2(12.5f, 6.1f) },
									Sans.MoveType.BOUNCE, 4f / 3f),
							new Sans(new Vector2(15.25f, 6.1f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(15.375f, 6.1f), new Vector2(13.625f, 6.1f),
											new Vector2(13.625f, 5.1f), new Vector2(15.375f, 5.1f),
											new Vector2(15.375f, 3.1f) },
									Sans.MoveType.BOUNCE, 6f / 3f),
							new Sans(
									new Vector2(11.125f, 4.85f), new Vector2(0.75f, 0.75f),
									new Vector2[] { new Vector2(11.125f, 4.85f), new Vector2(11.125f, 3.35f) },
									Sans.MoveType.BOUNCE, 4f / 3f),

							disposableWall2, disposableWall3,
							new Lever(new Vector2(4f, 3.7f), new Vector2(0.5f, 0.5f),
									new GameObject[] { disposableWall3 }),
							new Lever(new Vector2(7.75f, 6.45f), new Vector2(0.5f, 0.5f),
									new GameObject[] { disposableWall2 }),
							new Sign(new Vector2(14.5f, 0.85f), new Vector2(0.625f, 0.7f), new Vector2(-1, 0), 38,
									"epic"),
							new Exit(new Vector2(15.75f, 0.875f), new Vector2(0.5f, 0.75f)),
							new Shapiro(new Vector2(16f, 9f), Vector2.one(), 10, .1f),
							new Shapiro(new Vector2(16f, 1f), Vector2.one(), 15, .075f),
							new Shapiro(new Vector2(1f, 1f), Vector2.one(), 20, .05f),
							new Text(new Vector2(0.25f, 7.75f), Vector2.one(), 255, PConstants.LEFT, PConstants.CENTER,
									"Consolas", 30, String.format("\u2665 %d", attempts)) },
					new Player(3, 20, 1.15f, new Vector2(0.5f, 7.75f), new Vector2(0.5f, 0.5f), 210, 105, 30),
					"data/shapiro-song.wav", Clip.LOOP_CONTINUOUSLY);
		} else {
			// Load bug level.
			return new Level(
					new GameObject[] {
							new Sign(new Vector2(8, 1f), Vector2.one().mul(1f), 48, "libtards reSIGNed B-)" + "\n"),
							new Text(new Vector2(0.25f, 7.75f), Vector2.one(), PConstants.LEFT, PConstants.TOP,
									"Consolas", 30,
									String.format(String.format("\u2665 %d\n\u231b %s", jb.attempts,
											LocalTime.MIN.plusSeconds((int) (jb.getMilliTimePlayed() / 1000))
													.toString()))) },
					new Player(5, 20, STDJUMP, new Vector2(8, 4.5f), Vector2.one(), 210, 105, 30), "data/win-song.wav",
					0);
		}
	}
}
