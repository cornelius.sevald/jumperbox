package game;

import processing.core.PApplet;
import club.minnced.discord.rpc.*;

/** Et episk lille gamer-spil. */
public class JumperBox extends PApplet {

	public static boolean playing;

	// Is the user pressing the "a" or left arrow key?
	public boolean pressingLeft = false;
	// Is the user pressing the "d" or right arrow key?
	public boolean pressingRight = false;
	// Is the user pressing a jump key?
	public boolean pressingJump = false;

	public GameOptions gOptions;
	
	// Static singleton instance.
	static JumperBox jb = null;

	// Scoring.
	public int attempts = 0;
	double startTime;
	public double pauseTime = 0;
	public DiscordRPC discordLib;
	Thread discordThread;
	DiscordRichPresence presence;

	void setupDiscord() {
		discordLib = DiscordRPC.INSTANCE;
		String applicationId = "569328913407541269";
		String steamId = null;
		DiscordEventHandlers handlers = new DiscordEventHandlers();
		handlers.ready = (user) -> System.out.println("Discord RP is ready!");
		discordLib.Discord_Initialize(applicationId, handlers, true, steamId);
		presence = new DiscordRichPresence();
		presence.startTimestamp = System.currentTimeMillis() / 1000; // epoch second
		updateDiscordPresence("", "Le gaming \ud83d\ude0e", "sunglasses-sphere", "jumpman");
		// in a worker thread
		discordThread = new Thread(() -> {
			while (!Thread.currentThread().isInterrupted()) {
				discordLib.Discord_RunCallbacks();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException ignored) {
				}
			}
		}, "RPC-Callback-Handler");
		discordThread.start();
	}

	void updateDiscordPresence(String state, String details, String smallImgKey, String largeImgKey) {
		presence.state = state;
		presence.details = details;
		presence.smallImageKey = smallImgKey;
		presence.largeImageKey = largeImgKey;
		discordLib.Discord_UpdatePresence(presence);
	}

	public void settings() {
		System.out.println("Starting application...");
		jb = this;
		gOptions = new GameOptions(args);
		fullScreen();
	}

	public void setup() {
		setupDiscord();
		frameRate(60);
		startTime = System.currentTimeMillis();
		Level.loadLevel(0);
		playing = true;
		noCursor();
	}

	public void draw() {
		if (playing) {
			clear();
			background(255);
			Level.loadedLevel.updateObjects();
		}
	}

	@Override
	public void exit() {
		System.out.println("Exiting application...");
		discordThread.interrupt();
		discordLib.Discord_Shutdown();
		super.exit();
	}

	public void keyPressed() {
		if (key == 'a' || key == 'A' || keyCode == LEFT) {
			pressingLeft = true;
			if (playing) {
				Level.loadedLevel.getPlayer().facingRight = false;
			}
		}
		if (key == 'd' || key == 'D' || keyCode == RIGHT) {
			pressingRight = true;
			if (playing) {
				Level.loadedLevel.getPlayer().facingRight = true;
			}
		}
		if (key == ' ' || key == 'w' || key == 'W' || keyCode == UP) {
			pressingJump = true;
		}
	}

	public void keyReleased() {
		if (key == 'a' || key == 'A' || keyCode == LEFT) {
			pressingLeft = false;
			if (pressingRight == true) {
				if (playing) {
					Level.loadedLevel.getPlayer().facingRight = true;
				}
			}
		}
		if (key == 'd' || key == 'D' || keyCode == RIGHT) {
			pressingRight = false;
			if (pressingLeft == true) {
				if (playing) {
					Level.loadedLevel.getPlayer().facingRight = false;
				}
			}
		}
		if (key == ' ' || key == 'w' || key == 'W' || keyCode == UP) {
			pressingJump = false;
		}
	}

	public double getMilliTimePlayed() {
		return System.currentTimeMillis() - startTime - pauseTime;
	}
}
