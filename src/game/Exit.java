package game;

import processing.core.PConstants;

public class Exit extends GameObject {
	
	BoxCollider collider;
	
	public Exit(String name, Vector2 position, Vector2 scale) {
		super(name, position, scale);
		collider = new BoxCollider(this, true);
	}
	
	public Exit(Vector2 position, Vector2 scale) {
		this("Exit", position, scale);
	}
	
	@Override
	public void update() {
		show();
		if (Bounds.overlaps(collider.bounds(), Level.loadedLevel.player.getCollider().bounds())) {
			Level.loadedLevel.won = true;
		}
	}
	
	public void show() {
		Vector2 screenPosition = transform.toScreenPoint();
		Vector2 screenScale = transform.toScreenScale();
		
		jb.stroke(20);
		jb.strokeWeight(1);
		jb.fill(200, 50, 50);
		jb.rectMode(PConstants.CENTER);
		jb.rect(screenPosition.x, screenPosition.y, screenScale.x, screenScale.y);
	}
}
