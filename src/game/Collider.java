package game;

import java.util.*;

/** Base class for colliders. */
public abstract class Collider {
	/**The list of all colliders. */
	public static List<Collider> colliders = new ArrayList<Collider>();
	
	public boolean isTrigger;
	public Vector2 scale;
	public Vector2 offset;
	
	protected GameObject gameObject;
	
	/** Construct Collider with specified scale and offset. */
	public Collider(GameObject gameObject, boolean isTrigger, Vector2 scale, Vector2 offset) {
		this.gameObject = gameObject;
		this.isTrigger = isTrigger;
		this.scale = scale;
		this.offset = offset;
		colliders.add(this);
	}
	
	/** Construct Collider. */
	public Collider(GameObject gameObject, boolean isTrigger) {
		this(gameObject, isTrigger, Vector2.one(), Vector2.zero());
	}
	
	/** Remove this from the static list of colliders. */
	public void delete() {
		colliders.remove(this);
	}

	/** Construct Collider. */
	public Collider(GameObject gameObject) {
		this(gameObject, false);
	}
	
	/** Get the GameObject this collider is attached to. */
	public GameObject getGameObject() {
		return gameObject;
	}
	
	/** Get the bounding box encapsulating this collider. */
	public abstract Bounds bounds();
	
	/** Get the intersection points of a ray and this collider. */
	public abstract Vector2[] rayIntersection(Ray ray);
}