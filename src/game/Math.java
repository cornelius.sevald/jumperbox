package game;

public class Math {
	
	/** Return the signed integer of the number. */
	public static int sign(float num) {
		if (num < 0) {
			return -1;
		} if (num > 0) {
			return 1;
		}
		return 0;
	}
	
	/** Return the absolute value of the number. */
	public static float abs(float num) {
		return (num < 0) ? -num : num;
	}
	
	/** Return the smallest of the two floats. */
	public static float min(float a, float b) {
		return (a < b) ? a : b;
	}
	
	/** Return the largest of the two floats. */
	public static float max(float a, float b) {
		return (a > b) ? a : b;
	}
	
	/** Clamp the number, 'f', in between min and max.
	 * 
	 *  'min' may not be grater than 'max'.*/
	public static float clamp(float f, float min, float max) {
		if (min > max) {
			throw new java.lang.IllegalArgumentException("'min' may not be greater than 'max'.");
		}
		
		if (f < min) {
			f = min;
		} else if (f > max) {
			f = max;
		}
		return f;
	}
}
