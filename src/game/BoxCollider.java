package game;

public class BoxCollider extends Collider {
	
	/** Construct BoxCollider with specified scale and offset. */
	public BoxCollider(GameObject gameObject, boolean isTrigger, Vector2 scale, Vector2 offset) {
		super(gameObject, isTrigger, scale, offset);
	}
	
	/** Construct BoxCollider. */
	public BoxCollider(GameObject gameObject, boolean isTrigger) {
		super(gameObject, isTrigger);
	}
	
	/** Construct BoxCollider. */
	public BoxCollider(GameObject gameObject) {
		this(gameObject, false);
	}
	
	@Override
	public Bounds bounds() {
		Vector2 centre = gameObject.getTransform().position;
		Vector2 size = gameObject.getTransform().scale;
		
		Vector2 offsetCentre = new Vector2(centre.x + offset.x, centre.y + offset.y);
		Vector2 scaledSize = new Vector2(size.x * scale.x, size.y * scale.y);
		
		// As a transform can have negative scale, but bounds cannot have
		// a negative size, both coordinates should be multiplied by -1
		// if they are less than 0.
		size.x = size.x < 0f ? -size.x : size.x;
		size.y = size.y < 0f ? -size.y : size.y;
		
		return new Bounds(offsetCentre, scaledSize);
	}

	@Override
	public Vector2[] rayIntersection(Ray ray) {
		Bounds bounds = bounds();
		return bounds.rayIntersection(ray);
	}
}
