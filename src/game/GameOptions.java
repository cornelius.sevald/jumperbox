package game;

import org.apache.commons.cli.*;

/** Options for the game. */
public class GameOptions {
	
	public boolean music = true;
	
	/** Initialize a GameOptions instance based off passed arguments. */
	public GameOptions (String args[]) {
		Options options = getOptions();
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd;
		try {
			cmd = parser.parse( options, args);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		if (cmd.hasOption("M")) {
			music = false;
		} else {
			music = true;
		}
	}
	
	Options getOptions () {
		Options options = new Options();
		options.addOption("M", "no-music", false, "Disable music");
		return options;
	}
}
