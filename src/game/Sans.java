package game;

import processing.core.PApplet;
import processing.core.PImage;

public class Sans extends Enemy {
	
	/** The motion of the ocean, if the ocean is sans. */
	public enum MoveType {
		LOOP, BOUNCE
	}
	
	static String imagePath = "data/sans.png";

	// The points sans will move along.
	public Vector2[] points;
	MoveType moveType;
	float circuitDuration;
	
	Controller2D controller;
	PImage sansImage;
	float progress = 0;
	
	// Used when the moveType is equal BOUNCE.
	boolean movingForward = true;
	
	/** Create new Sans instance with a specified name, position and scale.
	 * 
	 *  The points define the path which sans will move along.
	 *  The move type specifies if sans loops around, or bounces back and fourth.*/
	public Sans(String name, Vector2 position, Vector2 scale, 
				Vector2[] points, MoveType moveType, float circuitDuration) {
		super(name, position, scale);
		
		collider = new BoxCollider(this, true, Vector2.one().mul(0.8f), Vector2.zero());
		this.points = points;
		this.moveType = moveType;
		this.circuitDuration = circuitDuration;
		
		controller = new Controller2D((BoxCollider) collider);
		sansImage = jb.loadImage(imagePath);
		sansImage.resize((int)Transform.toScreenScale(scale).x,
						 -(int)Transform.toScreenScale(scale).y);
	}
	
	public Sans(Vector2 position, Vector2 scale, Vector2[] points,
				MoveType moveType, float circuitDuration) {
		this("Sans", position, scale, points, moveType, circuitDuration);
	}
	
	public void update() {
		
		if (moveType == MoveType.LOOP) {
			doLoop();
		} else if (moveType == MoveType.BOUNCE) {
			doBounce();
		}
		
		show();
	}
	
	// Move according to the loop move type.
	void doLoop() {
		
		progress += 1 / (jb.frameRate * circuitDuration);
		if (progress >= 1) {
			progress -= 1;
		}
		
		int index = PApplet.floor(progress * points.length);
		float remainder = progress * points.length - index;
		
		Vector2 from = points[index];
		Vector2 to = index == (points.length - 1) ? points[0] : points[index + 1];
		Vector2 newPos = Vector2.lerp(from, to, remainder);
		
		checkForCollision(newPos.sub(transform.position));
		
		this.transform.position = newPos;
	}
	
	/** [NOT IMPLEMENTED] Move according to the bounce move type. */
	void doBounce() {
		
		if (movingForward) {
			progress += 1 / (jb.frameRate * circuitDuration);
			if (progress >= 1) {
				progress = 2 - progress;
				movingForward = false;
			}
		} else {
			progress -= 1 / (jb.frameRate * circuitDuration);
			if (progress <= 0) {
				progress = -progress;
				movingForward = true;
			}
		}
		
		int index = PApplet.floor(progress * (points.length - 1));
		float remainder = progress * (points.length - 1) - index;
		
		Vector2 from = points[index];
		Vector2 to = points[index + 1];
		Vector2 newPos = Vector2.lerp(from, to, remainder);
		
		checkForCollision(newPos.sub(transform.position));
		
		this.transform.position = newPos;
	}
	
	void checkForCollision(Vector2 velocity) {
		controller.doCollisions(velocity);
	}
	
	void show() {
		jb.imageMode(PApplet.CENTER);
		jb.image(sansImage, transform.toScreenPoint().x,
				 transform.toScreenPoint().y);
	}
}

