package game;

import processing.core.PFont;

public class Text extends GameObject {

	float r, g, b;
	int alignX, alignY;
	String fontName;
	float fontSize;
	PFont font;
	String message;

	public Text (String name,Vector2 position, Vector2 scale, float r, float g, float b, int alignX, int alignY, String fontName,
			float fontSize, String message) {
		super(name, position, scale);
		this.r = r;
		this.g = g;
		this.b = b;
		this.alignX = alignX;
		this.alignY = alignY;
		this.fontName = fontName;
		this.fontSize = fontSize;
		this.font = jb.createFont(fontName, fontSize);
		this.message = message;
	}
	
	public Text (Vector2 position, Vector2 scale, float r, float g, float b, int alignX, int alignY, String fontName,
			float fontSize, String message) {
		this("Text", position, scale, r, g, b, alignX, alignY, fontName, fontSize, message);
	}

	public Text (String name, Vector2 position, Vector2 scale, float value, int alignX, int alignY, String fontName,
			float fontSize, String message) {
		this(name, position, scale, value, value, value, alignX, alignY, fontName, fontSize, message);
	}
	
	public Text (Vector2 position, Vector2 scale, float value, int alignX, int alignY, String fontName,
			float fontSize, String message) {
		this(position, scale, value, value, value, alignX, alignY, fontName, fontSize, message);
	}

	public Text (String name, Vector2 position, Vector2 scale, int alignX, int alignY, String fontName,
			float fontSize, String message) {
		this(name, position, scale, 0, alignX, alignY, fontName, fontSize, message);
	}
	
	public Text (Vector2 position, Vector2 scale, int alignX, int alignY, String fontName,
			float fontSize, String message) {
		this(position, scale, 0, alignX, alignY, fontName, fontSize, message);
	}
	
	public Text (String name, Vector2 position, Vector2 scale, int alignX, int alignY,
			float fontSize, String message) {
		this(name, position, scale, 0, alignX, alignY, "Verdena", fontSize, message);
	}
	
	public Text (Vector2 position, Vector2 scale, int alignX, int alignY,
			float fontSize, String message) {
		this(position, scale, 0, alignX, alignY, "Verdena", fontSize, message);
	}

	@Override
	public void update () {
		show();
	}

	void show () {
		jb.fill(r, g, b);
		jb.textFont(font, fontSize);
		jb.textAlign(alignX, alignY);
		jb.text(message, transform.toScreenPoint().x, 
				transform.toScreenPoint().y + transform.toScreenScale().y);
	}

}
