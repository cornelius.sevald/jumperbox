package game;

import processing.core.PApplet;
import processing.core.PImage;

public class Shapiro extends Enemy {

	static String imagePath = "data/shapiro.png";
	
	Controller2D controller;
	PImage shapiroImage;
	
	Vector2 velocity = new Vector2();
	float speed;
	float accuracy;
	float maxVelocity;
	Ketchup k;
	
	// Used when the moveType is equal BOUNCE.
	boolean movingForward = true;
	
	/** Create new Shapiro instance with a specified name, position, scale, speed, accuracy and maximum velocity.
	 * 
	 * 'accuracy' should be between 0 and 1. */
	public Shapiro(String name, Vector2 position, Vector2 scale, float speed, float accuracy, float maxVelocity) {
		super(name, position, scale);
		
		this.speed = speed;
		this.accuracy = accuracy;
		this.maxVelocity = maxVelocity;
		
		collider = new BoxCollider(this, false, Vector2.one().mul(0.7f), Vector2.zero());
		controller = new Controller2D((BoxCollider) collider);
		shapiroImage = jb.loadImage(imagePath);
		shapiroImage.resize((int)Transform.toScreenScale(scale).x,
						 -(int)Transform.toScreenScale(scale).y);
	}
	
	/** Create new Shapiro instance with a specified position, scale, speed, accuracy and maximum velocity.
	 * 
	 * 'accuracy' should be between 0 and 1. */
	public Shapiro(Vector2 position, Vector2 scale, float velocity, float accuracy, float maxVelocity) {
		this("ben", position, scale, velocity, accuracy, maxVelocity);
	}
	
	/** Create new Shapiro instance with a specified position, scale, speed and accuracy.
	 * 
	 * 'accuracy' should be between 0 and 1. */
	public Shapiro(Vector2 position, Vector2 scale, float velocity, float accuracy) {
		this("ben", position, scale, velocity, accuracy, 6f);
	}
	
	public void update() {
			Player player = Level.loadedLevel.getPlayer();
			// The direction form shapiro to the player.
			Vector2 playerDir = player.transform.position.sub(transform.position);
			playerDir = playerDir.normalized();
			float differ = (1 - accuracy) * 2*PApplet.PI;
			Vector2 acceleration = playerDir.rotate(jb.random(-differ/2, differ/2)).mul(speed/jb.frameRate);
			velocity = velocity.add(acceleration);
			if (velocity.magnitude() > maxVelocity) {
				velocity = velocity.normalized().mul(maxVelocity);
			}
			
			controller.move(velocity.div(jb.frameRate));
			
			// If 'out of bounds' phase to other side of level.
			if (transform.position.x + transform.scale.x / 2f < 0) {
				transform.position.x = Transform.UPW - transform.position.x;
			} else if (transform.position.x - transform.scale.x / 2f > Transform.UPW) {
				transform.position.x = 0 - transform.position.x + Transform.UPW;
			}
			if (transform.position.y + transform.scale.y / 2f < 0) {
				transform.position.y = Transform.UPH - transform.position.y;
			} else if (transform.position.y - transform.scale.y / 2f > Transform.UPH) {
				transform.position.y = 0 - transform.position.y + Transform.UPH;
			}
			
			show();
	}
	
	void show() {
		jb.imageMode(PApplet.CENTER);
		jb.image(shapiroImage, transform.toScreenPoint().x,
				 transform.toScreenPoint().y);
	}

}
