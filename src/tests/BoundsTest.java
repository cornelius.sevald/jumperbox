package tests;

import game.Bounds;
import game.Ray;
import game.Vector2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class BoundsTest {

	/** Test the getExtents method of Bounds. */
	@Test
	void testGetExtents() {
		Bounds b1 = new Bounds(new Vector2(2f, 1f), new Vector2(5, 0.5f));
		Bounds b2 = new Bounds(new Vector2(1, -2f), new Vector2(6f, 6));
		Bounds b3 = new Bounds(new Vector2(-4, -3f), new Vector2(2, 3));
		Vector2 expected1 = new Vector2(2.5f, 0.25f);
		Vector2 expected2 = new Vector2(3, 3f);
		Vector2 notExpected1 = new Vector2(1, 3);
		
		assertAll(
				() -> assertTrue(b1.getExtents().equals(expected1)),
				() -> assertTrue(b2.getExtents().equals(expected2)),
				() -> assertFalse(b3.getExtents().equals(notExpected1)));
	}
	
	/** Test the getMax method of Bounds. */
	@Test
	void testGetMax() {
		Bounds b1 = new Bounds(new Vector2(2f, 1f), new Vector2(5, 0.5f));
		Bounds b2 = new Bounds(new Vector2(1, -2f), new Vector2(6f, 6));
		Bounds b3 = new Bounds(new Vector2(-4, -3f), new Vector2(2, 3));
		Vector2 expected1 = new Vector2(4.5f, 1.25f);
		Vector2 expected2 = new Vector2(4, 1f);
		Vector2 notExpected1 = new Vector2(0, 0);
		
		assertAll(
				() -> assertTrue(b1.getMax().equals(expected1)),
				() -> assertTrue(b2.getMax().equals(expected2)),
				() -> assertFalse(b3.getMax().equals(notExpected1)));
	}
	
	/** Test the getMin of Bounds. */
	@Test
	void testGetMin() {
		Bounds b1 = new Bounds(new Vector2(2f, 1f), new Vector2(5, 0.5f));
		Bounds b2 = new Bounds(new Vector2(1, -2f), new Vector2(6f, 6));
		Bounds b3 = new Bounds(new Vector2(-4, -3f), new Vector2(2, 3));
		Vector2 expected1 = new Vector2(-0.5f, 0.75f);
		Vector2 expected2 = new Vector2(-2, -5f);
		Vector2 notExpected1 = new Vector2(0, 0);
		
		assertAll(
				() -> assertTrue(b1.getMin().equals(expected1)),
				() -> assertTrue(b2.getMin().equals(expected2)),
				() -> assertFalse(b3.getMin().equals(notExpected1)));
	}
	
	/** Test the expand method of Bounds with a Vector2 argument. */
	@Test
	void testExpand1() {
		Bounds b1 = new Bounds(new Vector2(2f, 1f), new Vector2(5, 0.5f));
		Bounds b2 = new Bounds(new Vector2(1, -2f), new Vector2(6f, 6));
		Vector2 amount1 = new Vector2(-1, 1);
		Vector2 amount2 = new Vector2(0, 0.5f);
		Vector2 expectedSize1 = new Vector2(4, 1.5f);
		Vector2 expectedSize2 = new Vector2(6, 6.5f);
		
		b1.expand(amount1);
		b2.expand(amount2);
		
		assertAll(
				() -> assertTrue(b1.getSize().equals(expectedSize1)),
				() -> assertTrue(b2.getSize().equals(expectedSize2)));
	}
	
	/** Test the expand method of Bounds with a float argument. */
	@Test
	void testExpand2() {
		Bounds b1 = new Bounds(new Vector2(2f, 1f), new Vector2(5, 0.5f));
		Bounds b2 = new Bounds(new Vector2(1, -2f), new Vector2(6f, 6));
		float amount1 = 2f;
		float amount2 = -1.5f;
		Vector2 expectedSize1 = new Vector2(7, 2.5f);
		Vector2 expectedSize2 = new Vector2(4.5f, 4.5f);
		
		b1.expand(amount1);
		b2.expand(amount2);
		
		assertAll(
				() -> assertTrue(b1.getSize().equals(expectedSize1)),
				() -> assertTrue(b2.getSize().equals(expectedSize2)));
	}
	
	/** Test the contains method of Bounds. */
	@Test
	void testContains() {
		Bounds b1 = new Bounds(new Vector2(2f, 1f), new Vector2(5, 0.5f));
		Bounds b2 = new Bounds(new Vector2(1, -2f), new Vector2(6f, 6));
		Vector2 inside1 = new Vector2(0.5f, 1f);
		Vector2 inside2 = new Vector2(3, 0);
		Vector2 onEdge1 = new Vector2(-0.5f, 0.8f);
		Vector2 onEdge2 = new Vector2(4, 1);
		Vector2 notInside1 = new Vector2(-3, 0f);
		Vector2 notInside2 = new Vector2(0, -6);
		
		assertAll(
				() -> assertTrue(b1.contains(inside1)),
				() -> assertTrue(b2.contains(inside2)),
				() -> assertFalse(b1.contains(onEdge1)),
				() -> assertFalse(b2.contains(onEdge2)),
				() -> assertFalse(b1.contains(notInside1)),
				() -> assertFalse(b2.contains(notInside2)));
	}
	
	/** Test the isPointOnEdge method of Bounds. */
	@Test
	void testIsPointOnEdge() {
		Bounds b1 = new Bounds(new Vector2(2f, 1f), new Vector2(5, 0.5f));
		Bounds b2 = new Bounds(new Vector2(1, -2f), new Vector2(6f, 6));
		Vector2 inside1 = new Vector2(0.5f, 1f);
		Vector2 inside2 = new Vector2(3, 0);
		Vector2 onEdge1 = new Vector2(-0.5f, 0.8f);
		Vector2 onEdge2 = new Vector2(4, 1);
		Vector2 notInside1 = new Vector2(-3, 0f);
		Vector2 notInside2 = new Vector2(0, -6);
		
		assertAll(
				() -> assertFalse(b1.isPointOnEdge(inside1)),
				() -> assertFalse(b2.isPointOnEdge(inside2)),
				() -> assertTrue(b1.isPointOnEdge(onEdge1)),
				() -> assertTrue(b2.isPointOnEdge(onEdge2)),
				() -> assertFalse(b1.isPointOnEdge(notInside1)),
				() -> assertFalse(b2.isPointOnEdge(notInside2)));
	}
	
	/** Test the closestPoint method of Bounds. */
	@Test
	void testClosestPoint() {
		Bounds b = new Bounds(new Vector2(1f, -2f), new Vector2(5, 0.5f));
		// Point right of bounds.
		Vector2 pointR = new Vector2(3.8f, -1.8f);
		// Point over bounds.
		Vector2 pointO = new Vector2(3.4f, -1);
		// Point left of bounds.
		Vector2 pointL = new Vector2(-4.5f, -2);
		// Point under bounds.
		Vector2 pointU = new Vector2(-1.5f, -4);
		// Point right & over bounds.
		Vector2 pointRO = new Vector2(100, 100);
		// Point left & over bounds.
		Vector2 pointLO = new Vector2(-100, 100);
		// Point left & under bounds.
		Vector2 pointLU = new Vector2(-100, -100);
		// Point right & under of bounds.
		Vector2 pointRU = new Vector2(100, -100);
		// Point on left edge of bounds.
		Vector2 pointLE = new Vector2(-1.5f, -2.2f);
		// Point on right edge of bounds.
		Vector2 pointRE = new Vector2(3.5f, -1.9f);
		// Point inside bounds.
		Vector2 pointI = new Vector2(2.8f, -1.8f);
		// Expected points
		Vector2 expectedR = new Vector2(3.5f, -1.8f);
		Vector2 expectedO = new Vector2(3.4f, -1.75f);
		Vector2 expectedL = new Vector2(-1.5f, -2);
		Vector2 expectedU = new Vector2(-1.5f, -2.25f);
		Vector2 expectedRO = new Vector2(3.5f, -1.75f);
		Vector2 expectedLO = new Vector2(-1.5f, -1.75f);
		Vector2 expectedLU = new Vector2(-1.5f, -2.25f);
		Vector2 expectedRU = new Vector2(3.5f, -2.25f);
		Vector2 expectedLE = new Vector2(-1.5f, -2.2f);
		Vector2 expectedRE = new Vector2(3.5f, -1.9f);
		Vector2 expectedI = new Vector2(2.8f, -1.8f);
		
		assertAll(
				() -> b.closestPoint(pointR).equals(expectedR, 0.01f),
				() -> b.closestPoint(pointO).equals(expectedO, 0.01f),
				() -> b.closestPoint(pointL).equals(expectedL, 0.01f),
				() -> b.closestPoint(pointU).equals(expectedU, 0.01f),
				() -> b.closestPoint(pointRO).equals(expectedRO, 0.01f),
				() -> b.closestPoint(pointLO).equals(expectedLO, 0.01f),
				() -> b.closestPoint(pointLU).equals(expectedLU, 0.01f),
				() -> b.closestPoint(pointRU).equals(expectedRU, 0.01f),
				() -> b.closestPoint(pointLE).equals(expectedLE, 0.01f),
				() -> b.closestPoint(pointRE).equals(expectedRE, 0.01f),
				() -> b.closestPoint(pointI).equals(expectedI, 0.01f));
	}
	
	/** Test the rayIntersection method of Bounds. */
	@Test
	void testRayIntersection() {
		Bounds b = new Bounds(Vector2.zero(), Vector2.zero());
		b.setMinMax(new Vector2(-1, -1), new Vector2(2, 1));
		
		// Ray to the left of box pointing right.
		Ray ray1 = new Ray(new Vector2(-2, -0.5f), Vector2.right());
		// Ray inside box pointing left and up intersecting corner.
		Ray ray2 = new Ray(new Vector2(), new Vector2(-1, 1).normalized());
		// Ray over box pointing left and down.
		Ray ray3 = new Ray(new Vector2(1, 2), new Vector2(-1, -4).normalized());
		// Ray to the right of box pointing left with lower segment of box overlapping.
		Ray ray4 = new Ray(new Vector2(3, -1), Vector2.left());
		// Ray not intersecting box.
		Ray ray5 = new Ray(new Vector2(-2, 2), new Vector2(1, -4).normalized());
		Vector2[] expected1 = {new Vector2(-1, -0.5f), new Vector2(2, -0.5f)};
		Vector2[] expected2 = {new Vector2(-1, 1), new Vector2(-1, 1)};
		Vector2[] expected3 = {new Vector2(0.75f, 1), new Vector2(0.25f, -1)};
		Vector2[] expected4 = {new Vector2(2, -1), new Vector2(-1, -1)};
		Vector2[] expected5 = {};
		
		assertTrue(expected1[0].equals(b.rayIntersection(ray1)[0], 0.01f));
		assertTrue(expected1[1].equals(b.rayIntersection(ray1)[1], 0.01f));
		assertTrue(expected2[0].equals(b.rayIntersection(ray2)[0], 0.01f));
		assertTrue(expected2[1].equals(b.rayIntersection(ray2)[1], 0.01f));
		assertTrue(expected3[0].equals(b.rayIntersection(ray3)[0], 0.01f));
		assertTrue(expected3[1].equals(b.rayIntersection(ray3)[1], 0.01f));
		assertTrue(expected4[0].equals(b.rayIntersection(ray4)[0], 0.01f));
		assertTrue(expected4[1].equals(b.rayIntersection(ray4)[1], 0.01f));
		assertArrayEquals(expected5, b.rayIntersection(ray5));
	}
	
	/** Test that the class raises errors when needed. */
	@Test
	void testRaises() {
		Bounds b = new Bounds(new Vector2(1, -2), new Vector2(5, 0.5f));
		Vector2 centre = new Vector2(1, -2);
		Vector2 illegalSize = new Vector2(-3, 0);
		// Illegal pair of minimal maximal points.
		Vector2 illegalMin = new Vector2(2, -3);
		Vector2 illegalMax = new Vector2(1, -2f);
		// Legal pair of minimal maximal points.
		Vector2 legalMin = new Vector2(-2, -3);
		Vector2 legalMax = new Vector2(1, 2f);
		
		// Test construction with negative size.
		try {
			new Bounds(centre, illegalSize);
			fail("IllegalArgumentException expected");
		} catch (java.lang.IllegalArgumentException expected) {
			assertEquals("Negative coordinates of size is not allowed.", expected.getMessage());
		}
		// Test setting with negative extents.
		try {
			b.setExtents(illegalSize);
			fail("IllegalArgumentException expected");
		} catch (java.lang.IllegalArgumentException expected) {
			assertEquals("Negative coordinates of size is not allowed.", expected.getMessage());
		}
		// Test setting with illegal minimal and maximal points.
		try {
			b.setMinMax(illegalMin, illegalMax);
			fail("IllegalArgumentException expected");
		} catch (java.lang.IllegalArgumentException expected) {
			assertEquals("Both coordinates of the maximal point"
					+ " must be grater than those of the minimal.", expected.getMessage());
		}
		// Test setting with legal minimal and maximal points.
		try {
			b.setMinMax(legalMin, legalMax);
		} catch (java.lang.IllegalArgumentException expected) {
			fail("IllegalArgumentException NOT expected.");
		}
	}
}
